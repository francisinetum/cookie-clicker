# Cookie-clicker

## Contributeurs

* gysenlionel
* FrancisNtahimpera
* borisbecode
* thomasmelchers

***

<img src = "https://media.giphy.com/media/CXlLGiModdBneYJzYu/giphy.gif">

****

## Jour 1

### Difficultés

* se coordonner en groupe / trouver la bonne méthode de travail (ici Replit)

* le front est primordial en amont, sinon problèmes pour faire fonctionner

### Kiffs 

* les feuilles
les boutons en css, les rendre cliquables

### Suggestions 

les boutons > les transformer en carrés pour éviter le scroll sur la page (gauche, droite, en dessous ?)
diminuer la taille de l'arbre
changer la photo en forêt amazonnienne
idée : faire une croissance de l'arbre (petite pousse deviendra grand baobab)

